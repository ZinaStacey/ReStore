using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using API.Data;
using API.Entities;
using API.Extensions;
using API.RequestHelpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{

    public class ProductsController : BaseApiController
    {
        private readonly StoreContext _context;

        public ProductsController(StoreContext context)
        {
            _context = context;

        }

        [HttpGet]//this adds it to swagger so we can test it out though the fields are required to be entered for it to work in swagger
        public async Task<ActionResult<PagedList<Product>>> GetProducts([FromQuery] ProductParams productParams)
        {
            //this is the order it is shoween to the user for them to search up their products
            //var query = _context.Products.Sort(ProductParams.OrderBy).Search(ProductParams.SearchTerm).Filter(ProductParams.Brands, ProductParams.Types).AsQueryable();
            var query = _context.Products.Sort(productParams.OrderBy).Search(productParams.SearchTerm).Filter(productParams.Brands, productParams.Types).AsQueryable();
            var products = await PagedList<Product>.ToPagedList(query, productParams.PageNumber, productParams.PageSize);

            Response.AddPaginationHeader(products.MetaData);
            return products;
        }


        [HttpGet("{id}")]//api/products/3
        public async Task<ActionResult<Product>> GetProduct(int id)
        {

            var products = await _context.Products.FindAsync(id);
            if (products == null)
            {
                return NotFound();
            }
            return products;
        }

        [HttpGet("filters")]
        public async Task<IActionResult> GetFilters()
        {
            var brands = await _context.Products.Select(p => p.Brand).Distinct().ToListAsync();
            //seperate query to database
            var types = await _context.Products.Select(p => p.Type).Distinct().ToListAsync();
            //gives 2 lists to provid list of things to filter on
            return Ok(new { brands, types });
        }
    }
}

/*
        [HttpGet]//this code workd to excute the command in swagger to show the products
        public async Task<ActionResult<List<Product>>> GetProducts()
        {
            //this is the order it is shoween to the user for them to search up their products
            //var query = _context.Products.Sort(orderBy).Search(searchTerm).Filter(brands, types).AsQueryable();

            //return await query.ToListAsync();
            return await _context.Products.ToListAsync();
        }
        */

/*
[HttpGet]//this adds it to swagger so we can test it out but woudnt show the products when exucuted without typing the fields
//how ever in the products id header in swagger where u enter a number of the product to look at, that still works
public async Task<ActionResult<List<Product>>> GetProducts(string orderBy)
{
    //this is the order it is shoween to the user for them to search up their products
    //var query = _context.Products.Sort(orderBy).Search(searchTerm).Filter(brands, types).AsQueryable();
    var query = _context.Products.AsQueryable();
    query = orderBy switch
    {
        "price" => query.OrderBy(p => p.Price),
        "priceDesc" => query.OrderByDescending(p => p.Price),
        _ => query.OrderBy(p => p.Name)
    };
    // return query;
    return await query.ToListAsync();
}
*/