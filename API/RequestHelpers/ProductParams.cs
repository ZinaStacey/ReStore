using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.RequestHelpers
{
    public class ProductParams : PaginationParams
    {
        public string? OrderBy { get; set; }

        public string? SearchTerm { get; set; }

        public string? Types { get; set; }

        public string? Brands { get; set; }

        /*//i added this constructor so that the non nullable could go away but ill try the ? in the string upabove
        public ProductParams(string orderBy, string searchTerm, string brands, string types){
             OrderBy = orderBy;
             SearchTerm = searchTerm;
             Types = types;
             Brands = brands;
        }
        */

    }
}