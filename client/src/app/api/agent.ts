import axios, { AxiosError, AxiosResponse } from "axios";
import { request } from "http";
import { toast} from "react-toastify";
import { history } from "../..";
import { PaginatedResponse } from "../models/pagination";
//import { Toast } from "react-toastify/dist/components";

const sleep = () => new Promise(resolve => setTimeout(resolve, 500));

axios.defaults.baseURL = 'http://localhost:5044/api/';
axios.defaults.withCredentials = true;

const responseBody = (response: AxiosResponse) => response.data;

const requests = {
    get: (url: string, params?: URLSearchParams) => axios.get(url, {params}).then(responseBody),
    post: (url: string, body: {}) => axios.post(url, body).then(responseBody),
    put: (url: string, body: {}) => axios.put(url, body).then(responseBody),
    delete: (url: string) => axios.delete(url).then(responseBody),
}

const Catalog = {
    list: (params: URLSearchParams) => requests.get('products', params),
    details: (id: number) => requests.get(`products/${id}`),
    fetchFilters: ()=> requests.get('products/filters')
}

axios.interceptors.response.use(async response =>{
    await sleep();
    //keep the pagaination as small letter p not a capital its the console fault if thats what it thinks
    //console.log(response);//use this to double check things are the same
    const pagination = response.headers['pagination'];
    if(pagination){
        //create new instints of class
        response.data = new PaginatedResponse(response.data, JSON.parse(pagination));
        //this console log checking to see if metadata data added
        //console.log(response);
        return response;
    }
    return response
}, (error: AxiosError) => {
    const {data, status} = error.response!;
    switch (status) {
        case 400:
            if(data.errors){
                const modelStateErrors: string[] = [];
                for(const key in data.errors){
                    if(data.errors[key]){
                        modelStateErrors.push(data.errors[key]);
                    }
                }
                throw modelStateErrors.flat();
            }
            toast.error(data.title);
            break;
        case 401:
            toast.error(data.title);
            break;
        case 500:
            history.push({pathname: '/server-error', state:{error:data}});
            break;
        default:
            break;
    }
    return Promise.reject(error.response);
})

const TestErrors = {
    get400Error: () => requests.get('buggy/bad-request'),
    get401Error: () => requests.get('buggy/unauthorised'),
    get404Error: () => requests.get('buggy/not-found'),
    get500Error: () => requests.get('buggy/server-error'),
    getValidationError: () => requests.get('buggy/validation-error'),
}

const Basket = {
    get: () => requests.get('basket'),
    addItem: (productId: number, quantity =1) => requests.post(`basket?productID=${productId}&quantity=${quantity}`, {}),
    removeItem: (productId: number, quantity =1) => requests.delete(`basket?productID=${productId}&quantity=${quantity}`),

}

const agent = {
    Catalog,
    TestErrors,
    Basket
}

export default agent;
