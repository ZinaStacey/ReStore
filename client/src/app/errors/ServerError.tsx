import { Button, Container, Divider, Paper, Typography } from "@mui/material";
import { useHistory, useLocation } from "react-router-dom";

export default function ServerError(){

const history = useHistory();
//anther hook
const {state} = useLocation<any>();

    return (
        <Container component={Paper}>
            {state?.error ? (<><Typography variant="h3" color='error' gutterBottom>{state.error.title}</Typography>
            <Divider></Divider>
                <Typography>{state.error.detail || 'internal server error'}</Typography>
            </>):(
                <Typography variant="h5" gutterBottom>Server error second part of turnery in server error</Typography>
            )}
            <Button onClick={() => history.push('/catalog')}>Go Back to the store</Button>
        </Container>
    )
}