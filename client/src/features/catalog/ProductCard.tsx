import { LoadingButton } from "@mui/lab";
import { ListItem, ListItemAvatar, Avatar, ListItemText, Button, Card, CardActions, CardContent, CardMedia, Typography, CardHeader } from "@mui/material";
import { useState } from "react";
import { Link } from "react-router-dom";
import agent from "../../app/api/agent";
import { useStoreContext } from "../../app/context/StoreContext";
import { Product } from "../../app/models/product";
import { useAppDispatch, useAppSelector } from "../../app/store/configureStore";
import { currencyFormat } from "../../app/util/util";
import { addBasketItemAsync, setBasket } from "../basket/basketSlice";

interface Props{
    products: Product;
}

export default function ProductCard({products}: Props){

  const {status} = useAppSelector(state => state.basket);
  const dispatch = useAppDispatch();
  

    return (
        <Card >
            <CardHeader avatar={<Avatar sx={{bgcolor: 'secondary.main'}}>{products.name.charAt(0).toUpperCase()}</Avatar>}
             title={products.name}
             titleTypographyProps={{sx:{fontWeight: 'bold', color: 'secondary.main'}}}>                
            </CardHeader>
        <CardMedia
          component="img"
          height="140"
          sx={{height: 140, objectFit: 'contain', bgcolor: 'primary.light'}}
          image={products.pictureUrl}
          title={products.name}          
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom color='secondary' variant="h5" component="div">
            {currencyFormat(products.price)}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {products.brand} / {products.type}
          </Typography>
        </CardContent>
        <CardActions>
          <LoadingButton loading={status.includes('pendingAddItem' + products.id)} onClick={() => dispatch(addBasketItemAsync({productId: products.id}))} size="small">Add to cart</LoadingButton>
          <Button component={Link} to={`/catalog/${products.id}`} size="small">View</Button>
        </CardActions>
      </Card>
    )
}